package gobforwardauth_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/gitlab-org/opstrace/gobforwardauth"
)

func TestDemo(t *testing.T) {
	cfg := gobforwardauth.CreateConfig()

	ctx := context.Background()
	next := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {})

	handler, err := gobforwardauth.New(ctx, next, cfg, "gobforwardauth-plugin")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost", nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(recorder, req)

	//TODO(prozlach): Add tests, for now we just make sure that plugin builds

}
